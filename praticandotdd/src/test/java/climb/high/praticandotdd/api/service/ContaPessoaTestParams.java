package climb.high.praticandotdd.api.service;

import climb.high.praticandotdd.domain.dto.resource.ContaPessoaCadastroDto;
import climb.high.praticandotdd.domain.dto.resource.PessoaResultadoBuscaDto;
import climb.high.praticandotdd.domain.entity.ContaPessoa;
import lombok.Data;

@Data
public class ContaPessoaTestParams {
    private ContaPessoaCadastroDto cadastroDto;
    private ContaPessoa contaPessoaAdicionada;
    private PessoaResultadoBuscaDto pessoaResultadoBusca;
}