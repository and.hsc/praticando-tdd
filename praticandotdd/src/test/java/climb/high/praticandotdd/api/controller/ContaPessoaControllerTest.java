package climb.high.praticandotdd.api.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import climb.high.praticandotdd.api.service.ContaPessoaService;
import climb.high.praticandotdd.domain.dto.resource.ContaPessoaCadastroDto;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

//import * org.springframework.test.web.servlet.request.MockMvcRequestBuilder.get;;
/**
 * Colinha:
 * 
 * import static mockito.ArgumentMatchers.any; import static
 * mockito.Mockito.times; import static mockito.Mockito.verify; import static
 * mockito.Mockito.verifyNoMoreInteractions; import static mockito.Mockito.when;
 * import static
 * org.springframework.test.web.servlet.request.MockMvcRequestBuilder.get;
 * import static
 * org.springframework.test.web.servlet.result.MockMvcRequestBuilder.content;
 * import static
 * org.springframework.test.web.servlet.result.MockMvcRequestBuilder.jsonPath;
 * import static
 * org.springframework.test.web.servlet.result.MockMvcRequestBuilder.status;
 * import static com.googlecode.catchexception.CatchException.*;
 * 
 * when(_service.method(param))
 * 
 * verify(_service, times(1)).method(any(Colinha.class));
 * 
 * @InjectMocks private Controller _controller;
 * 
 * @MockBean private Service _serviceMock;
 * 
 *           ResultActions request = _mockMvc.perform(get("uri/1"));
 * 
 *           request.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON))
 *           .andExpect(jsonPath(".data").value(1)).andReturn();
 * 
 * 
 *           ArgumentCaptor<Colinha> captor= new
 *           ArgumentCaptor.forClass(Colinha.class);
 * 
 *           verify(_service, times(1)).method(captor.capture());
 * 
 *           Colinha cola = captor.getValue()
 *
 */
@WebMvcTest(ContaPessoaController.class)
public class ContaPessoaControllerTest {

	private final PodamFactory podamFactory = new PodamFactoryImpl();

	@Autowired
	private MockMvc mockMvc;

	@InjectMocks
	private ContaPessoaController controller;

	@MockBean
	private ContaPessoaService contaPessoaService;

	@Test
	public void post_criarContaPessoa_returnCreated() throws Exception {

		// Arrange - given
		var contaPessoaCadastroDto = podamFactory.manufacturePojo(ContaPessoaCadastroDto.class);

		var mapper = new ObjectMapper();

		var body = mapper.writeValueAsString(contaPessoaCadastroDto);
	
		ArgumentCaptor<ContaPessoaCadastroDto> argumentCaptor = ArgumentCaptor.forClass(ContaPessoaCadastroDto.class);

		Mockito.doNothing().when(contaPessoaService).adicionarContaPessoa(argumentCaptor.capture());

		// Act - when
		ResultActions request = mockMvc
				.perform(post("/api/contapessoa").contentType(MediaType.APPLICATION_JSON).content(body));

		// Assert - then
		request.andExpect(status().isCreated());
		
		
		Mockito.verify(contaPessoaService, Mockito.times(1)).adicionarContaPessoa(argumentCaptor.capture());
		var actual = argumentCaptor.getValue();
		var actualBody = mapper.writeValueAsString(actual);
		
		assertEquals(body, actualBody);

	}
	
//	@Test
	public void post_criarContaPessoa_returnUnprocessableEntity() throws Exception {

		// Arrange - given
		var contaPessoaCadastroDto = podamFactory.manufacturePojo(ContaPessoaCadastroDto.class);
		contaPessoaCadastroDto.setTipoPessoa("AAAA");
		
		var mapper = new ObjectMapper();

		var body = mapper.writeValueAsString(contaPessoaCadastroDto);
	
		// Act - when
		ResultActions request = mockMvc
				.perform(post("/api/contapessoa").contentType(MediaType.APPLICATION_JSON).content(body));

		// Assert - then
		request.andExpect(status().isUnprocessableEntity());
		
		Mockito.verify(contaPessoaService, Mockito.never()).adicionarContaPessoa(Mockito.any());
		
	}
	
//	@Test
	public void post_criarContaPessoa_returnPessoaNotFound() {

	}

	// @Test
	public void get_buscarContaPorId_returnOk() {

	}

	// @Test
	public void put_atualizarTipoParticipacaoConta_returnContaNotFound() {

	}

	// @Test
	public void put_atualizarTipoParticipacaoConta_returnPessoaNotFound() {

	}

	// @Test
	public void put_atualizarTipoParticipacaoConta_returnNoContent() {

	}

	// @Test
	public void get_buscaContaPorCnpj_returnOk() {

	}
}
