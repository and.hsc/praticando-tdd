package climb.high.praticandotdd.domain.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;

@Data
@Entity
public class Conta {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int contaId;
	private String agencia;
	private String numero;
	private String digito;
	@OneToMany(mappedBy = "pessoa")
	private Set<ContaPessoa> pessoas = new HashSet<ContaPessoa>();
}
