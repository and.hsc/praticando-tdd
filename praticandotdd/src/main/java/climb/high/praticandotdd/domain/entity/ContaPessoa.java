package climb.high.praticandotdd.domain.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity
@IdClass(ContaPessoaId.class)
public class ContaPessoa implements Serializable {
	public ContaPessoa() {
		final var TITULAR_STRING = "Titular";
		this.tipoParticipacaoConta = TITULAR_STRING;
	}

	// public ContaPessoa() {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@ManyToOne(optional = false)
	@JoinColumn(name = "contaId")
	private Conta conta;

	@Id
	@ManyToOne(optional = false)
	@JoinColumn(name = "pessoaId")
	private Pessoa pessoa;
	private String tipoParticipacaoConta;
}
