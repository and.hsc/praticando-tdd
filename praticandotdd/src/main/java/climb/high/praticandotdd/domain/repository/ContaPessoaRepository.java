package climb.high.praticandotdd.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import climb.high.praticandotdd.domain.entity.ContaPessoa;
import climb.high.praticandotdd.domain.entity.ContaPessoaId;

@Repository
public interface ContaPessoaRepository extends JpaRepository<ContaPessoa, ContaPessoaId> {

}
