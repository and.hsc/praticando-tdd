package climb.high.praticandotdd.domain.dto.resource;

import lombok.Data;

@Data
public class PessoaResultadoBuscaDto {
	private int pessoaId;
	private String nome;
	private String tipoPessoa;
	private String documento;
	private String tipoParticipacaoConta; 
}
