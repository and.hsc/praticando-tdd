package climb.high.praticandotdd.domain.exception;

import java.util.Date;

public class ResponseException {
	private Date timestamp;
	private String[] messages;
	private String details;

	public ResponseException(String[] messages, String details) {
		this.timestamp = new Date();
		this.messages = messages;
		this.details = details;
	}

	public ResponseException(String message, String details) {
		this.timestamp = new Date();
		this.messages = new String[] { message };
		this.details = details;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public String[] getMessages() {
		return messages;
	}

	public String getDetails() {
		return details;
	}
}
