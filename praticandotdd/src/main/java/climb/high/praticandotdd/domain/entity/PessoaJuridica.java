package climb.high.praticandotdd.domain.entity;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@PrimaryKeyJoinColumn(name = "pessoaId")
public class PessoaJuridica extends Pessoa {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String cnpj;
	
	@Override
	public void setDocumento(String cnpj) {
		this.cnpj = cnpj;
	}
}
