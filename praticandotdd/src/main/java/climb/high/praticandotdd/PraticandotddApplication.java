package climb.high.praticandotdd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PraticandotddApplication {

	public static void main(String[] args) {
		SpringApplication.run(PraticandotddApplication.class, args);
	}

}
